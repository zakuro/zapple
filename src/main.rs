use std::env;

fn main() {
    let mut args = env::args();
    if args.len() != 2 {
        eprintln!("Usage: zapple <code>\n");
        return
    }
    println!(r#"
.intel_syntax noprefix
.global main

main:
    mov rax,    {}
    ret
    "#, args.nth(1).unwrap());
}
