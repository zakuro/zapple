#!/bin/bash

try-zapple() {
  expected="$1"
  input="$2"

  ./target/debug/zapple "$input" > tmp.s
  gcc -o tmp tmp.s
  ./tmp
  actual="$?"

  if [[ "$expected" == "$actual" ]]
  then
    echo "OK, $expected is returned"
  else
    echo "expected $expected, got $actual"
  fi
}

cargo build

try-zapple 0 0
try-zapple 42 42
